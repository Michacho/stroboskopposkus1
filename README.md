# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://Michacho@bitbucket.org/Michacho/stroboskop.git
```

Naloga 6.2.3:
((UVELJAVITEV))
https://bitbucket.org/Michacho/stroboskop/commits/a9308a4890d2c74c201c1105eada73067cda0bdb

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Michacho/stroboskop/commits/6bc515f6d5dc4534e6eb638aec078e40c2c03af3?at=izgled

Naloga 6.3.2:
https://bitbucket.org/Michacho/stroboskop/commits/699192758b5d1ada77227353aeb42902c2965bd9

Naloga 6.3.3:
https://bitbucket.org/Michacho/stroboskop/commits/f483add8246e93d6afd44127b24a1913dfb08954

Naloga 6.3.4:
https://bitbucket.org/Michacho/stroboskop/commits/e91001fc28d130eda94ddebe7de39a98c9f3fd8c

Naloga 6.3.5:


```
 git add jscolor.js
 git commit -a -m "Priprava potrebnih JavaScript knjižnic"
 git push origin master
  git checkout -b izgled
  git add stroboskop.html
  git commit -m "Dodajanje stilov spletne strani"
  git push origin izgled
  git add stroboskop.html
  git commit -m "Desna poravnava besedil"
  git push origin izgled
  git add stroboskop.html
  git commit -m "Dodajanje gumba za dodajanje barv"
  git push origin izgled
  git add stili.css
  git add stroboskop.html
  git commit -m "Dodajanje robov in senc"
  git push origin izgled
  git checkout master
  git push origin master
  

  
  
  
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV))

Naloga 6.4.2:
((UVELJAVITEV))

Naloga 6.4.3:
((UVELJAVITEV))

Naloga 6.4.4:
((UVELJAVITEV))